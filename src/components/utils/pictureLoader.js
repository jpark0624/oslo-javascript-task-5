import LETTER_A from '../../assets/a.png';
import LETTER_B from '../../assets/b.png';
import LETTER_C from '../../assets/c.png';
import LETTER_D from '../../assets/d.png';
import LETTER_E from '../../assets/e.png';
import LETTER_F from '../../assets/f.png';
import LETTER_G from '../../assets/g.png';
import LETTER_H from '../../assets/h.png';
import LETTER_I from '../../assets/i.png';
import LETTER_J from '../../assets/j.png';
import LETTER_K from '../../assets/k.png';
import LETTER_L from '../../assets/l.png';
import LETTER_M from '../../assets/m.png';
import LETTER_N from '../../assets/n.png';
import LETTER_O from '../../assets/o.png';
import LETTER_P from '../../assets/p.png';
import LETTER_Q from '../../assets/q.png';
import LETTER_R from '../../assets/r.png';
import LETTER_S from '../../assets/s.png';
import LETTER_T from '../../assets/t.png';
import LETTER_U from '../../assets/u.png';
import LETTER_V from '../../assets/v.png';
import LETTER_W from '../../assets/w.png';
import LETTER_X from '../../assets/x.png';
import LETTER_Y from '../../assets/y.png';
import LETTER_Z from '../../assets/z.png';

export const LETTERS = {
    a: LETTER_A,
    b: LETTER_B,
    c: LETTER_C,
    d: LETTER_D,
    e: LETTER_E,
    f: LETTER_F,
    g: LETTER_G,
    h: LETTER_H,
    i: LETTER_I,
    j: LETTER_J,
    k: LETTER_K,
    l: LETTER_L,
    m: LETTER_M,
    n: LETTER_N,
    o: LETTER_O,
    p: LETTER_P,
    q: LETTER_Q,
    r: LETTER_R,
    s: LETTER_S,
    t: LETTER_T,
    u: LETTER_U,
    v: LETTER_V,
    w: LETTER_W,
    x: LETTER_X,
    y: LETTER_Y,
    z: LETTER_Z
}