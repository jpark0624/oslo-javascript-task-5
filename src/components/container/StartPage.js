import React, {useState, useEffect} from 'react';
import {getStorage, setStorage} from '../utils/localStorage';
import { Link } from 'react-router-dom';

const StartPage = () => {
    const [ isLoggedIn, setIsLoggedIn ] = useState(false);
    const [ name, setName ] = useState(''); 

    useEffect(() => {
        setIsLoggedIn(getStorage('user'));
    }, []);

    const onNameChange = (input) => setName(input.target.value.trim());
    const onLoginClicked = () => {
        setStorage('user', name);
        setStorage('words', []);
        setIsLoggedIn(true);
    }

    return  (
        <div className="container">
            <h2>Please enter your user ID to log in</h2>
            <input className="input" type="text" placeholder="Enter your user ID" onChange={onNameChange}/>
            <Link to='/translator'>
            <button type='button' className='btn' onClick={onLoginClicked}>Log in</button>
            </Link>
        </div>
    )
};

export default StartPage;