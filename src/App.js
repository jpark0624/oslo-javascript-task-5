import React from 'react';
import './App.css';
import { BrowserRouter as Router, 
  Switch, 
  Route, 
  Redirect 
} from 'react-router-dom';
import StartPage from './components/container/StartPage';
import TranslationPage from './components/container/TranslationPage';
import ProfilePage from './components/container/ProfilePage';
import NotFoundPage from './components/container/NotFoundPage';

function App() {
  return (
    <Router>
      <div className='App'>
      <h1 className="header">Welcome to the sign language translator app</h1>
      <Switch>
        <Route exact path='/'>
          <Redirect to='/home'/>
        </Route>
        <Route exact path='/home' component={StartPage}/>
        <Route path='/translator' component={TranslationPage}/>
        <Route path='/profile' component={ProfilePage}/>
        <Route path='/*' component={NotFoundPage}/>
      </Switch>
    </div>
    </Router>
  );
}

export default App;