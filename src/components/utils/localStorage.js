export const setStorage = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
}

export const getStorage = (key) => {
    const storedValue = localStorage.getItem(key);
    if (storedValue) {
        return JSON.parse(storedValue);
    } else {
        return false; 
    }
}