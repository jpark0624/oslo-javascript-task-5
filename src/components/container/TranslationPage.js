import React, { useState, useEffect } from 'react';
import { getStorage, setStorage } from '../utils/localStorage';
import {LETTERS} from '../utils/pictureLoader';
import { Link } from 'react-router-dom';

const TranslationPage = () => {

    const [name, setName] = useState('');
    const [word, setWord] = useState('');
    const [translated, setTranslated] = useState([]);

    useEffect(() => {
        setName(getStorage('user'));
    }, []);

    const handleTextChange = (input) => setWord(input.target.value.trim().toLowerCase());

    const handleOnClick = () => {
        let translate = [];
        let chars = word.trim().split('');
        for (let i = 0; i < chars.length; i++) {
            if (!/[a-zA-Z]/g.test(chars[i])) {
                translate.push(chars[i]);
            } else {
                translate.push(LETTERS[chars[i]]);
            }
        }
        const currentWords = getStorage('words') || [];
        if (currentWords.length < 10) {
            currentWords.push(word)
        } else {
            currentWords.shift()
            currentWords.push(word)
        }
        setStorage('words', [...currentWords]);
        setTranslated(translated);




        const pictures = translate.map((char, index) => <img src={char} alt="letters" key={index}/>);
        setTranslated(pictures);
    }

    return (
        <div className="container">
            <div className="headerBar">
                <Link to="/profile">
                <button className="btn-profile" type="button">My profile</button>
                </Link>
            </div>
            <form className="form">
                <input className='input' type='text' placeholder='Type here and press translate button' 
                        minLength="1" maxLength="40" onChange={handleTextChange}/>
                <button className='btn' type="button" onClick={handleOnClick}>Translate</button>
                <div className="output">
                </div>
                {translated}
            </form>
        </div>
    );
}

export default TranslationPage;