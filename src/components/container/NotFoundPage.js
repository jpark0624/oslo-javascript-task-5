import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => {
    return  (
        <div className="container">
            <h2>404 PAGE NOT FOUND</h2>
            <Link className="link" to='/home'> Back to start page</Link>
        </div>
    )
};

export default NotFound;