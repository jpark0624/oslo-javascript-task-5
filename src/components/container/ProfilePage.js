import React, { useState, useEffect } from 'react';
import { getStorage, setStorage } from '../utils/localStorage';
import { Link } from 'react-router-dom';

function ProfilePage() {
    const [ name, setName ] = useState('');
    const onLogoutClicked = () => localStorage.clear();
    const onClearClicked = () => setStorage('words', []);

    useEffect(() => {
        setName(getStorage('user'));
    }, []);
    
    let words = getStorage('words').map((word, index) => { return (
        <div key={index}>{word}</div>
        )})

    return (
        <div className="container">
            <h2>My translate history:</h2>
            <h3>{words}</h3>
            <Link to="/translator"> 
                <button className="btn" type="button">Back</button>
            </Link>
            <Link to="/translator">
                <button className="btn-clear" type="button" onClick={onClearClicked}>Clear history</button>
            </Link>
            <Link to='/home'>
                <button className="logout" type='button' onClick={onLogoutClicked}>Log out</button>
            </Link>
        </div>
    );
}

export default ProfilePage;